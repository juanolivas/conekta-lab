# ConektaLab - Notifications

## Getting started
Conektalab is a Rails application running on Ruby 2.4.0 and Rails 5.1.4

To get started you will need to install Ruby in your system. The easy way to do this in OS X is via [Homebrew](http://brew.sh) and [Ruby Build](https://github.com/rbenv/ruby-build#readme).

## Setting up Conektalab
Clone this repo and move into the project directory.

Execute inside the project directory the following commands, those commands will:
- Install Ruby dependencies
- Install Redis

```
> bundle install
> brew install redis
```

### Starting the web application
To start conektalab run the following command:

```
> rails s
```

Open new terminal and execute:

```
> bundle exec sidekiq
```

Conektalab now it's running!

## Sending a request
```
curl -X POST http://localhost:3000/api/v1/notification -d "notification=123"
``` 


## Running test
To execute test run the following command:

```
rake spec
```
