class Api::V1::NotificationsController < ApplicationController
  protect_from_forgery

  def post_notification
    @notification = Notification.where(identifier: params[:notification], status: true).first
    if @notification && recent_notification?
      render plain: "Notification #{@notification.identifier} created less than 5 minutes ago."
    else
      NotifyWorker.perform_async params[:notification]
      render plain: "New notification created"
    end
  end

  def recent_notification?
    @notification.updated_at > 5.minutes.ago
  end
end
