class NotifyWorker
  include Sidekiq::Worker
  sidekiq_options retry: 0,
                  unique_for: 5.minutes

  def perform(notification_id)
    notification = Notification.where(identifier: notification_id).first_or_create
    notification.status = true
    notification.save
    notification.touch
    puts "Notification created, sending to user..."
  end
end
