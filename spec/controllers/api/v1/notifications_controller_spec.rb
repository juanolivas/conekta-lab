require "rails_helper"
require "sidekiq/testing"

describe Api::V1::NotificationsController, type: :controller do
  let(:notification) do
    OpenStruct.new(status: true,
                   identifier: "123",
                   updated_at: "2017-09-28 02:12:53.012688")
  end

  context "When a notification it's recieved" do
    context "When an unprocessed notification" do
      it "Send notification to webhook", sidekiq: :fake! do
        expect do
          NotifyWorker.perform_async(notification)
        end.to change(NotifyWorker.jobs, :size).by(1)
        expect(response.status).to eq 200
      end
    end

    context "When processed notification it's received" do
      it "Notify that is a recent notification" do
        expect(notification[:status]).to eq true
        expect(response.status).to eq 200
      end
    end
  end
end
