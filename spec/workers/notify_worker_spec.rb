require 'spec_helper'
require 'rails_helper'

describe NotifyWorker do
  let(:notification_id){ "123" }
  let(:notification_params) do
    {
      status: true,
      identifier: "123"
    }
  end

  it "gets enqueued correctly", sidekiq: :fake! do
    expect do
      NotifyWorker.perform_async(notification_id)
    end.to change(NotifyWorker.jobs, :size).by(1)
  end

  it "Create the notification" do
    expect(Notification.where(identifier: notification_id).first).to be_nil
    expect {
    Notification.where(status: true, identifier: "123").first_or_create
  }.to change{Notification.count}.by(1)
  end
end
